<?php

namespace Modules\Core\Providers;

use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider {
    protected $defer = false;

    public function boot() {
        $this->loadTranslationsFrom(
            realpath(__DIR__ . '/../Resources/lang'),
            strtolower('Core')
        );
        $this->loadViewsFrom(
            __DIR__ . '/../Resources/views',
            strtolower('Core')
        );
    }

    public function register() {
       $this->mountRoutes();
       $this->registerConfig();
    }

    protected function mountRoutes() {
    	require_once(__DIR__ . '/../Http/routes.php');
    }

    protected function registerConfig() {
        $this->app['config']->set(
            'admin.menu',
            array_merge_recursive(
                require __DIR__.'/../Config/menu.php',
                $this->app['config']->get(
                    'admin.menu',
                    []
                )
            )
        );
        $this->app['config']->set(
            'admin.permissions',
            array_merge_recursive(
                require __DIR__.'/../Config/permissions.php',
                $this->app['config']->get(
                    'admin.permissions',
                    []
                )
            )
        );
    }
}
