<?php
return [
    'id'        => '#',
    'login'     => 'Username',
    'email'     => 'E-Mail',
    'lastname'  => 'Last name',
    'firstname' => 'First name',
    'actions'   => 'Actions',
    'name'      => 'Name'
];
?>
