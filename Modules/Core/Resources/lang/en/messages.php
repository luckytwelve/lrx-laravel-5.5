<?php

return [

    //Success
    '0.1.1' 	=> 'You have been successfully authorized', //'Вы успешно авторизированы.',
    '0.1.2' 	=> 'Message with instructions has been sent to your email', //'Сообщение с дальнейшими инструкциями отправлено Вам на E-Mail.',
    '0.1.3'		=> 'Password has been successfully changed', //'Пароль успешно изменен.',


    '1.1.1' 	=> 'User is successfully deleted', //'Укзанный пользователь успешно удален.',
    '1.1.2' 	=> 'User is successfully added', //'Ваш пользователь успешно добавлен.',
    '1.1.3'		=> 'Data is successfully updated', //'Запись успешно обновлена.',


    '2.1.1' 	=>  'New group is successfully created', //'Новая группа успешно создана.',
    '2.1.2'		=>  'Group is successfully updated', //'Группа успешно отредактирована.',
    '2.1.3'		=>  'Group is successfully removed', //'Группа успешно удалена.',

    '3.1.1'		=>  'New permissions are successfully added', //'Новые права доступа успешно добавлены.',
    '3.1.2'		=>  'Data is successfully removed', //'Запись успешно удалена.',
    '3.1.3'		=>  'Data is successfully updated', //'Запись успешно обновлена.',

    //Error

    // System
    '0.2.1' 	=>  'Incorrect login or password', //'Пользователя с такой комбинацией логин/пароль не найдено.',
    '0.2.2' 	=>  'One or more fields are filled in incorrectly', //'Одно или несколько полей заполнено не верно.',
    '0.2.2'		=>  'Email is required', //'Поле E-Mail должно быть заполнено.',
    '0.2.3'		=>  'Email is incorrect', //'Поле E-Mail заполнено не верно.',
    '0.2.4'		=>  "User with this email doesn't exist", //'Пользователя с таким E-Mail не существует.',
    '0.2.5'		=>  "Processor doesn't have required data", //'Обработчик не получил все необходимые данные.',


    // Users
    '1.2.1'		=> "User doesn't exist in database", //'Такой пользователь отсутствует в базе данных.',
    '1.2.2' 	=> "User can't be deleted for unknown reasons", //'Не удалось удалить пользователя по неизвестным причинам.',
    '1.2.3'		=> "Processor doesn't have required data", //'Обработчик не получил все необходимые данные.',
    '1.2.4'		=> 'One or more fields are filled in incorrectly', //'Одно или несколько полей заполнено не верно.',
    '1.2.5' 	=> "Passwords don't match", //'Введенные пароли не идентичны.',
    '1.2.6' 	=> 'User has to belong to at least one group', //'Пользователь должен принадлежать хотя бы к одной группе.',
    '1.2.7'		=> 'User with this login already exists', //'Пользователь с таким Логином уже существует.',
    '1.2.8'		=> 'User with this e-mail already exists', //'Пользователь с таким E-Mail уже существует.',
    '1.2.9'		=> "One or more of selected groups don't exit", //'Одной или несколько из указаных груп не существует.',
    '1.2.10'	=> "User can't be added for unknown reasons", //'Не удалось добавить пользователя по неизвесным причинам.',
    '1.2.11'	=> "Passwords don't match the format", //'Введенные пароли не соответствуют формату.',
    '1.2.12'	=> 'Critical error happened while data update. Please, try again.', //'Произошла критическая ошибка при обновлении записи. Попробуйте снова.',
    '1.2.13'	=> "You can't remove the system administrator", //'НЕЛЬЗЯ УДАЛЯТЬ АДМИНИСТРАТОРА СИСТЕМЫ!!!',
    '1.2.14'	=> 'Login is required', //'Поле логин должно быть заполнено.',
    '1.2.15'	=> 'Login should contain only latin characters, numbers, underscore and dash', //'Поле логин должно содержать только латинские символы, цифры, знаки подчёркивания (_) и дефисы (-).',
    '1.2.16'	=> 'Login should be longer than 2 characters', //'Значение логина должно быть более 2 символов.',
    '1.2.17'	=> 'Login should be less or equal to 32 characters', //'Значение логина должно быть меньше или равно 32 символам.',
    '1.2.18'	=> 'Email is required', //'Поле E-Mail должно быть заполнено.',
    '1.2.19'	=> 'Email is incorrect', //'Не корректно заполнено поле E-Mail.',
    '1.2.20'	=> 'Last name is required', //'Поле фамилия должно быть заполнено.',
    '1.2.21'    => 'Field should contain only latin and cyrillic characters, numbers, underscore and dash', //'Поле должно содержать только латинские и кириллические символы, цифры, знаки подчёркивания (_) и дефисы (-).',
    '1.2.22'	=> 'Last name should be longer than 2 characters', //'Значение фамилии должно быть более 2 символов.',
    '1.2.23'	=> 'Last name should be less or equal to 32 characters', //'Значение фамилии должно быть меньше или равно 32 символам.',
    '1.2.24'	=> 'First name is required', //'Поле имя должно быть заполнено.',
    '1.2.25'	=> 'Field should contain only latin and cyrillic characters, numbers, underscore and dash', //'Поле должно содержать только латинские и кириллические символы, цифры, знаки подчёркивания (_) и дефисы (-).',
    '1.2.26'	=> 'First name should be longer than 2 characters', //'Значение имени должно быть более 2 символов.',
    '1.2.27'	=> 'First name should be less or equal to 32 characters', //'Значение имени должно быть меньше или равно 32 символам.',

    // userGroups
    '2.2.1'		=> 'Group name is incorrect', //'Не верно заполнено название группы.',
    '2.2.2'		=> 'One or few fields are filled incorrect', //'Одно или несколько полей заполнено не верно.',
    '2.2.3'		=> 'New group should have at least on permission', //'Новая группа должна иметь минимум одно право доступа.',
    '2.2.4'		=> "One or few selected permissions don't exist", //'Одного или несколько из указаных прав доступа не существует.',
    '2.2.5' 	=> "Selected group doesn't exist", //'Указаной группы не существует.',
    '2.2.6'		=> 'Critical error happened while data edit', //'Произошла критическая ошибка при редактировании записи.',
    '2.2.7'		=> 'Critical error happened while group delete. Please, try again', //'Произошла критическая ошибка при удалении группы. Попробуйте снова.',
    '2.2.8'		=> 'Group with the same name exists', //'Группа с таким названием уже существует.',
    '2.2.9'		=> 'Critical error happened while data add', //'Произошла критическая ошибка при добавлении записи.',
    '2.2.10'	=> "You can't remove the administrator's group", //'НЕЛЬЗЯ УДАЛЯТЬ ГРУППУ АДМИНИСТРАТОРОВ!!!',
    '2.2.11'	=> 'Name is required', //'Название должно быть заполнено.',
    '2.2.12'	=> 'Name should contain only latin characters, numbers, underscore and dash', //'Название должно содержать только латинские символы, цифры, знаки подчёркивания (_) и дефисы (-).',
    '2.2.13'	=> 'Name should be longer than 2 characters', //'Значение названия должно быть более 2 символов.',
    '2.2.14'	=> 'Name should be less or equal to 32 characters', //'Значение названия должно быть меньше или равно 64 символам.',


    // userPermissions
    '3.2.1'		=> "Processor doesn't have required data", //'Обработчик не получил все необходимые данные.',
    '3.2.2'		=> 'One or more fields are filled in incorrectly', //'Одно или несколько полей заполнено не верно.',
    '3.2.3'		=> 'Permission with this name already exists', //'Права с таким названием уже существуют.',
    '3.2.4'		=> 'Critical error happened while permission add. Please, try again.', //'Произошла критическая ошибка при добавлении новых прав. Попробуйте снова.',
    '3.2.5'		=> "Data doesn't exist", //'Указаной записи не существует.',
    '3.2.6'		=> 'Critical error happened while permission delete. Please, try again.', //'Произошла критическая ошибка при удалении прав. Попробуйте снова.',
    '3.2.7'		=> 'Critical error happened while data update. Please, try again.', //'Произошла критическая ошибка при обновлении записи. Попробуйте снова.',
    '3.2.8'		=> 'Name is required', //'Название должно быть заполнено.',
    '3.2.9'		=> 'Name should contain only latin characters, numbers, underscore and dash', //'Название должно содержать только латинские символы, цифры, знаки подчёркивания (_) и дефисы (-).',
    '3.2.10'	=> 'Name should be longer than 2 characters', //'Значение названия должно быть более 2 символов.',
    '3.2.11'	=> 'Name should be less or equal to 32 characters', //'Значение названия должно быть меньше или равно 32 символам.',
    '3.2.12'	=> 'Description is required', //'Описание должно быть заполнено.',
    '3.2.13'	=> 'Description should be longer than 2 characters', //'Значение описания должно быть более 2 символов.',
    '3.2.14'	=> 'Description should be less or equal to 32 characters', //'Значение описания должно быть меньше или равно 250 символам.',

];