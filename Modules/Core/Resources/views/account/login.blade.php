<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LRX - Admin panel</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/admin/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/admin/css/theme.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/admin/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/admin/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/admin/css/extras.min.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('/assets/admin/js/theme.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/admin/js/components.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/admin/js/core.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/admin/js/modules.min.js') }}"></script>

    <script type="text/javascript">
        ADM.managerPath = '/admin'

        $(document).ready(function() {
          ADM.modules.get('account_login')._init();
        });
    </script>
</head>

<body>
	<!-- Page container -->
	<div class="page-container login-container">
		<!-- Page content -->
		<div class="page-content">
			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Content area -->
				<div class="content" data-module="account_login">
                    <div class="content">
    					<!-- Simple login form -->
    					<form action="{{ route('admin-account-login') }}" id="loginForm">
    						<div class="panel panel-body login-form">
    							<div class="text-center">
    								<div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
    								<h5 class="content-group">Login to your account <small class="display-block">Enter your credentials below</small></h5>
    							</div>

    							<div class="form-group has-feedback has-feedback-left">
    								<input type="text" class="form-control" placeholder="Username" name="login">
    								<div class="form-control-feedback">
    									<i class="icon-user text-muted"></i>
    								</div>
    							</div>

    							<div class="form-group has-feedback has-feedback-left">
    								<input type="password" class="form-control" placeholder="Password" name="password">
    								<div class="form-control-feedback">
    									<i class="icon-lock2 text-muted"></i>
    								</div>
    							</div>

    							<div class="form-group">
    								<button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 position-right"></i></button>
    							</div>

                                <div class="content-divider text-muted"><span></span></div>

                                <center>
    								<a href="/" class="btn btn-link btn-block"><i class="icon-arrow-left13 position-left"></i> Back to site</a>
    							</center>
    						</div>
    					</form>
    					<!-- /simple login form -->
				    </div>
				</div>
				<!-- /content area -->
			</div>
			<!-- /main content -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->
</body>
</html>
