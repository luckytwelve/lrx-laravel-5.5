<form action="<?=route('admin-managers-groups-save')?>" id="ManagersGroupsForm" method="POST" data-callback="managersGroups::AfterFormSend" data-module="managersGroupsForm">
    <input type="hidden" name="id" value="{!! $id !!}">
    <div class="modal-body with-padding">
        <div class="form-group">
            <div class="row">
                <div class="col-sm-12">
                    <label>Group name</label>
                    <input type="text" placeholder="" id="ManagerGroupName" name="ManagerGroupName"  class="form-control" value="{!! $groupName !!}">
                </div>
            </div>
        </div>
        <h6>Access rights</h6>
        <div class="panel panel-default">
            <div class="panel-body">
            <a href="" class="btn pull-right btn-info btn-xs" id="checkAllPermissions">Select all</a>
            <?php $Permissions = config( 'admin.permissions' );
            foreach($Permissions as $name => $group): ?>
                <h6><?=$name;?>:</h6>
                <div class="row" style="padding-bottom:30px">
                <?php foreach($group as $key => $permission): ?>
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"
                                       name="ManagerGroupsPermissionsList[<?=$permission['name']?>]"
                                       class="styled"
                                       <?=!isset($PermissionList[$permission['name']])?'':'checked'?> ><?=$permission['description']?>
                            </label>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</form>
