<!-- Page header -->
<div class="page-header" style="margin-top:15px;">
    <div class="page-header-content">
        <div class="heading-elements">
            <div class="heading-btn-group">
            </div>
        </div>
    </div>

</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <div data-module="managersGroups">
        {!! $table !!}
    </div>
</div>