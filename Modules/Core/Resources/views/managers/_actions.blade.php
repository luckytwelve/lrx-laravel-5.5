<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>

        <ul class="dropdown-menu dropdown-menu-right">
            <li>
                <a href="{{
                    sprintf(
                        '#%s?id=%d',
                        route('admin-managers-edit'),
                        $row->id
                    )
                }}">
                    <i class="icon-cog4"></i> Edit
                </a>
            </li>
            <li>
                <a
                    href="{{
                        sprintf(
                            '#%s?id=%d',
                            route('admin-managers-delete'),
                            $row->id
                        )
                    }}"
                    data-callback="managers::ReloadTable"
                    data-confirm="{{
                        sprintf(
                            'Are you sure you want to delete item with ID %d?',
                            $row->id
                        )
                    }}"
                >
                    <i class="icon-remove"></i> Delete
                </a>
            </li>
        </ul>
    </li>
</ul>
