<form action="{{ route('admin-managers-save') }}" method="POST" data-callback="managers::AfterFormSend" data-module="managersForm">
    @if(isset($Manager))
        <input type="hidden" name="id" value="{!! $Manager->id !!}">
    @endif

    <div class="modal-body with-padding">
        <div class="form-group">
            <label>Username</label>
            <input type="text" name="login" class="form-control" value="{{ $Manager->login or '' }}">
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control" value="{{ $Manager->email or '' }}">
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-sm-6">
                    <label>First name</label>
                    <input type="text" name="first_name" class="form-control" value="{{ $Manager->first_name or '' }}">
                </div>
                <div class="col-sm-6">
                    <label class="control-label">Last Name</label>
                    <input type="text" name="last_name" class="form-control" value="{{ $Manager->last_name or '' }}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-sm-6">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control">
                </div>
                <div class="col-sm-6">
                    <label>Confirm password</label>
                    <input type="password" name="repassword" class="form-control">
                </div>
            </div>
        </div>
        <div class="form-group">
            <h6>Groups</h6>
            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="" class="btn pull-right btn-info btn-xs" id="checkAllGroups">Select all</a>
                    @if(isset($Groups) && count($Groups) > 0)
                        @foreach($Groups as $Group)
                            <div class="col-md-4">
                                <div class="checkbox">
                                    <label>
                                        @if(isset($Relations) && in_array($Group->id, $Relations->modelKeys()))
                                            <input type="checkbox" name="groups[{!! $Group->id !!}]" class="styled" checked /> {!! $Group->name !!}
                                        @else
                                            <input type="checkbox" name="groups[{!! $Group->id !!}]" class="styled" /> {!! $Group->name !!}
                                        @endif
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</form>
