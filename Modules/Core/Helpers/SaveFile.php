<?php

namespace Modules\Core\Helpers;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SaveFile
{

    public static function getFileName(UploadedFile $file)
    {
        $name = strtolower(self::transliterate($file->getClientOriginalName()));
        $image_name = md5(microtime() . $name) .'.'. $file->getClientOriginalExtension();
        return $image_name;
    }

    public static function save(UploadedFile $file, $path, $image_name = null, $additional_dirs = FALSE)
    {

        //add ending slash if necessary
        if (substr($path, -1) != '/') $path .= '/';

        $name = strtolower(self::transliterate($file->getClientOriginalName()));

        if(empty($image_name)){
            $image_name = md5(microtime() . $name) .'.'. $file->getClientOriginalExtension();
        }

        //place file in folders to decrease the load on filesystem
        $hash = md5(time() . $name);
        if ($additional_dirs)
            $directories = substr($hash, 0, 2) . '/' . substr($hash, 2, 2) . '/' . substr($hash, 4, 2) . '/';
        else
            $directories = '';

        $success = $file->move($path . $directories, $image_name);
        return [
            'success' => $success,
            'name' => $success ? ( $directories . $image_name ) : ''
        ];
    }

    public static function transliterate($string) {
        $string = iconv(mb_detect_encoding($string, mb_detect_order(), true), "UTF-8", $string);
        $string = trim ($string);
        $converter = array(
            '"' => '',   "'" => '',
            ' ' => '_',   '.' => '.',   ',' => '_',
            'à' => 'a',   'á' => 'b',   'â' => 'v',
            'ã' => 'g',   'ä' => 'd',   'å' => 'e',
            '¸' => 'e',   'æ' => 'zh',  'ç' => 'z',
            'è' => 'i',   'é' => 'y',   'ê' => 'k',
            'ë' => 'l',   'ì' => 'm',   'í' => 'n',
            'î' => 'o',   'ï' => 'p',   'ð' => 'r',
            'ñ' => 's',   'ò' => 't',   'ó' => 'u',
            'ô' => 'f',   'õ' => 'h',   'ö' => 'c',
            '÷' => 'ch',  'ø' => 'sh',  'ù' => 'sch',
            'ü' => '',    'û' => 'y',   'ú' => '',
            'ý' => 'e',   'þ' => 'yu',  'ÿ' => 'ya',
            'À' => 'A',   'Á' => 'B',   'Â' => 'V',
            'Ã' => 'G',   'Ä' => 'D',   'Å' => 'E',
            '¨' => 'E',   'Æ' => 'Zh',  'Ç' => 'Z',
            'È' => 'I',   'É' => 'Y',   'Ê' => 'K',
            'Ë' => 'L',   'Ì' => 'M',   'Í' => 'N',
            'Î' => 'O',   'Ï' => 'P',   'Ð' => 'R',
            'Ñ' => 'S',   'Ò' => 'T',   'Ó' => 'U',
            'Ô' => 'F',   'Õ' => 'H',   'Ö' => 'C',
            '×' => 'Ch',  'Ø' => 'Sh',  'Ù' => 'Sch',
            'Ü' => '',    'Û' => 'Y',   'Ú' => '',
            'Ý' => 'E',   'Þ' => 'Yu',  'ß' => 'Ya',
        );
        return strtr($string, $converter);
    }
}