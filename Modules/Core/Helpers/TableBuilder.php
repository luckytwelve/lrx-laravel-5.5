<?php
namespace Modules\Core\Helpers;

class TableBuilder {
    protected $id;
    protected $name;
    protected $description;
    protected $buttons;
    protected $columns;
    protected $view;

    public function __construct($id) {
        $this->id = $id;
        $this->view = view('admin.layouts._partials.datatable');
    }

    public function setName( $name ) {
        $this->name = $name;
        return $this;
    }

    public function setButtons($buttons = []) {
        $this->buttons = $buttons;
        return $this;
    }

    public function setColumns($columns = []) {
        $this->columns = $columns;
        return $this;
    }

    public function build() {
        return $this->view->with([
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'columns' => $this->buildColumns(),
            'buttons' => $this->buttons
        ])->render();
    }

    private function buildColumns() {
        $columns = [];

        foreach( $this->columns  as $value) {
            $width = empty( $value['width']) ? '': ' width="' . $value['width'] . '" ';
            $columns[] = '<th ' . $width . '>' . $value['title'] . '</th>';
        }

        return implode('', $columns);
    }
}
