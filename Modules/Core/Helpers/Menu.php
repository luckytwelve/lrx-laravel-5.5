<?php
namespace Modules\Core\Helpers;
/**
 * Class Menu
 */

class Menu {
    protected $TemplateEngin;

    static $defaults = [
        'index' => 0,
        'text' => '',
        'childs' => null,

    ];
    /**
     * @param $TemplateEngin
     */
    /*public function __construct( $TemplateEngin ){
        $this->TemplateEngin = $TemplateEngin;
    }*/


    /**
     * @param $tag
     * @param $attributes
     * @return string
     */
    public static function generateTag( $tag, $attributes ){

        unset( $attributes['childs'] , $attributes['text']);
        $attr = [];
        if ( is_callable( $attributes['a.href'] )  ) $attributes['a.href'] = $attributes['a.href']();
        foreach($attributes as $attribut=>$value) {
            if ( strpos($attribut, $tag.'.') === 0 ){

                $attr[] = str_replace( $tag.'.' , '',$attribut) . '="' . (is_array($value) ? implode(' ', $value) : $value) . '"';
            }
        }
        return '<'.$tag.' '.implode(' ',$attr).'>{content}</'.$tag.'>';
    }


    /**
     * @param $menu
     */
    public static function build( $menu ){


        $li = [];

        usort($menu, function($a, $b){
            $a = isset($a['index']) ? $a['index'] : 0 ;
            $b = isset($b['index']) ? $b['index'] : 0 ;
            return $a > $b;
        });

        foreach( $menu as $key=>$node ){
            if(isset($node['permissions'])) {
                //dd(managerHasOneOfPermissions($node['permissions'], \Auth::manager()->get()->id));
                if(!Permission::managerHasPermission($node['permissions'], \Auth::guard('manager')->user()->id)) {
                    continue;
                }
            }
            if(isset($node['role']) && !userIsRole($node['role'])) {
                continue;
            }

            $_node = static::_normalize($node);

            $_li = static::generateTag('li', $_node);


            if ( !empty($_node['tpl']) ){



            } else {
                $_a = static::generateTag('a', $_node);
            }


            if ( is_array($_node['childs']) ){
                $_ul = static::generateTag('ul', $_node);
                $inner = static::build( $_node['childs']);
                $_ul = str_replace('{content}', $inner, $_ul);
            } else {
                $_ul = '';
            }

            $li[] = str_replace( '{content}', static::langParser($_node['text']), str_replace('{content}',$_a.$_ul,$_li) );
        }

        return implode('', $li);
    }

    static function langParser($text = ' ') {
        $text = ' ' . $text;

        if(strpos($text, '{lang-')) {
            $text = preg_replace_callback("'{lang-(.*?)}'msi", function($matches) {
                $return = '';

                for($i = 1; $i < count($matches); $i++) {
                    $return .= trans($matches[$i]);
                }

                return $return;

            }, $text);
        }

        return $text;
    }

    /**
     * @param $array
     * @return mixed
     */
    static function _normalize( $array ){
        return array_merge( static::$defaults , $array);
    }
}
