<?php
namespace Modules\Core\Helpers;

class Notify {
    public static function success($content, $data = []) {
        $data['type'] = 'success';
        $data['text'] = $content;

        return static::response($data);
    }

    public static function info($content, $data = []) {
        $data['type'] = 'info';
        $data['text'] = $content;

        return static::response($data);
    }

    public static function warning($content, $data = []) {
        $data['type'] = 'warning';
        $data['text'] = $content;

        return static::response($data);
    }

    public static function error($content, $data = []) {
        $data['type'] = 'error';
        $data['text'] = $content;

        return static::response($data);
    }

    private static function response( $data ) {
        $data[ 'text' ]  = static::parse( $data['text'], $data );

        return response()->json([
           'notification' => $data
        ]);
    }

    private static function parse( $template, $fields ) {
        if ( is_callable( $template ) ){
            $template = $template( $fields );
        } else {
            foreach($fields as $field=>$value){
                $template = str_replace( '{'.$field.'}', $value, $template);
            }
        }
        return $template;
    }
}
