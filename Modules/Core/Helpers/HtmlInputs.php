<?php
namespace Modules\Core\Helpers;

class HtmlInputs
{
    public static function getHtml($Settings,$param)
    {
        if (count($Settings) > 0):

        $output ='
                    <div class="col-sm-6">

                        <label>'.$Settings['text'].':</label>
                        '.GetInput::GetInput( $Settings, $param ).'
                    </div>

            ';
        endif;

        return $output;
    }
}

?>