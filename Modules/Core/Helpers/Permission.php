<?php
namespace Modules\Core\Helpers;

use \Modules\Core\Entities as Models;

use Session;


class Permission {

    public static function hasPermission( $type = 'manager', $permissions, $userID = false){
        switch($type){
            case 'manager':
                static::managerHasPermission( $permissions, $userID);
                break;
        }
    }

    public static function managerHasPermission( $permissions, $userID = null){
        if(!$userID) $userID = \Auth::guard('manager')->user()->id;
        $permissions = is_array($permissions) ? $permissions : Array( $permissions );


        $groups = \Auth::guard('manager')->user()->groups()->get();

        foreach( $groups as $group) {

            /*
            * Return true if user is an administrator "GOD mode" - ON
            */
            if($group->id == 1) return true;

            $allPermissions = static::getAllGroupPermissions( 'manager', $group->id );

            if(is_array($permissions)) {
                if(isset($allPermissions[ $group->id ]) && count($allPermissions[ $group->id ]) > 0){
                    foreach($allPermissions[ $group->id ] as $groupPermission){
                        if ( in_array($groupPermission, $permissions ) ) return true;
                    }
                }
            } else {
                foreach($allPermissions[ $group->id ] as $groupPermission){
                    if ( $permissions == $groupPermission ) return true;
                }
            }
        }
        return false;
    }

    //Get all cached permissions
    public static function getAllGroupPermissions( $type = 'manager', $group_id = 0){
        $type = $type == 'manager' ? 'manager' : 'user' ;
        //Session::forget( 'permissions-'.$type );
        $permissions = Session::get( 'permissions-'.$type );


        //dd($permissions);
        if ( empty( $permissions )){
            $allPermissions = $type == 'manager' ? Models\ManagersGroupsPermissionsRelations::where('group_id', '=', $group_id)->get() :  '';

            foreach( $allPermissions as $groupPermission) {
                $permissions[$groupPermission->group_id][] = $groupPermission->permission_id;
            }
            Session::put( 'permissions-'.$type , $permissions);
        }

        return $permissions;
    }
}
