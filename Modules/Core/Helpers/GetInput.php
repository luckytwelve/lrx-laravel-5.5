<?php
namespace Modules\Core\Helpers;

class GetInput
{
    public static function GetInput( $setting, $param ){

        $default = [
            'name'				=> '',
            'text' 				=> '',
            'type' 				=> '',
            'default' 			=> '',
            'class' 			=> '',
            'attributes'		=> '',
            'script'			=> '',
            'area-size' 		=> '',
            'content'			=> '',
            'filemanager-id'	=> '',
            'values'			=> []
        ];

        $setting = array_merge($default , $setting);
        if ( is_callable( $setting['values'])) {
            $setting['values'] = $setting['values']();
        }

        return call_user_func("\Modules\Core\Inputs\\" . ucfirst($setting['type']) . "::handle", $setting, $param);

    }
}