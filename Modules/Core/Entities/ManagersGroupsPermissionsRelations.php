<?php namespace Modules\Core\Entities;
   
use Illuminate\Database\Eloquent\Model;

class ManagersGroupsPermissionsRelations extends Model {

    protected $fillable = [];
    public static $unguarded = true;
    protected $table = 'managers_groups_permissions_relations';

}