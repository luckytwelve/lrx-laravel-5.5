<?php
namespace Modules\Core\Entities;

use Illuminate\Foundation\Auth\User;
use Modules\Core\Entities\ManagerGroup;
use Illuminate\Database\Eloquent\SoftDeletes;

class Manager extends User
{
    protected $fillable     = [];
    protected $dateFormat   = 'U';
    protected $table        = 'managers';
    protected $hidden       = [];
    protected $dates        = ['deleted_at'];
    protected static $unguarded = true;

    public function groups() {
        return $this->belongsToMany(ManagerGroup::class, 'managers_groups_relations', 'manager_id', 'group_id');
    }
}
