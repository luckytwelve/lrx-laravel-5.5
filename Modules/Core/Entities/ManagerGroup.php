<?php
namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Manager;

class ManagerGroup extends Model {
    public $timestamps = false;

    protected $table = 'managers_groups';
    protected static $unguarded = true;

    public function managers() {
        return $this->belongsToMany(Manager::class, 'managers_groups_relations', 'group_id', 'manager_id');
    }
}
