<?php
namespace Modules\Core\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Modules\Core\Helpers\Notify;
use Illuminate\Routing\Controller;
use Modules\Core\Entities\Managers;
use Illuminate\Support\Facades\View;

class AccountController extends Controller {
	public function login() {
        return view('core::account.login');
	}

	public function loginJson(Request $request) {
		$input = $request->only(['login', 'password']);
        $validation = \Validator::make($input, [
            'login' => 'required|alpha_num|min:3|max:16',
            'password' => 'required|min:6|max:32'
        ]);

		if($validation->fails()) {
			return Notify::error(
				current($validation->messages()->toArray())[0]
			);
        }

        $Admin = \Auth::guard('manager')->attempt([
            'login'         => $input['login'],
            'password'      => $input['password']
        ], true);

        if(!$Admin) {
            return Notify::error( trans('lrx::messages.0.2.1') );
        }

        return Notify::success( trans('lrx::messages.0.1.1') );
	}

	public function logout() {
		Auth::guard('manager')->logout();
		session()->forget('permissions-manager');

		return redirect()->away(route('admin-account-login'));
	}
}
