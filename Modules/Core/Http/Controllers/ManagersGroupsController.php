<?php
namespace Modules\Core\Http\Controllers;

use Illuminate\Routing\Controller,
    Illuminate\Http\Request,
    Modules\Core\Helpers\Notify,
    Modules\Core\Entities\Manager,
    Modules\Core\Entities\ManagerGroup,
    Modules\Core\Helpers\TableBuilder;

class ManagersGroupsController extends Controller {

    public function lists() {
        $view = \View::make('core::managersGroups.lists');
        $table = new TableBuilder('managersGroups');
        $table->setName('Administrators groups list');

        if (managerHasPermission('managers-groups-create')) {
            $table->setButtons(['<a href="#' . route('admin-managers-groups-edit') . '" class="pull-right btn btn-success">Create</a>']);
        }
        $table->setColumns([
            [ 'title'=> trans('core::tables.id'), 'width'=> '5%' ],
            [ 'title'=>trans('core::tables.name'), 'width'=> '20%' ],
            [ 'title'=>trans('core::tables.actions'), 'width'=> '5%' ]
        ]);

        $view->with('table', $table->build());

        return $view;
    }

    public function listsJson()
    {
        $query = \DB::table('managers_groups')->select(['managers_groups.id as id', 'managers_groups.name as name']);

        return \Datatables::of($query)->editColumn('actions', function ($row) {
            $return = [];
            array_push($return, '<ul class="icons-list">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-menu9"></i>
						</a>
						<ul class="dropdown-menu dropdown-menu-right">');
            if (managerHasPermission('managers-groups-edit'))
                array_push($return, '<li><a href="#'.route('admin-managers-groups-edit').'?id='.$row->id. '"><i class="icon-cog4"></i> Edit</a></li>');
            if (managerHasPermission('managers-groups-delete'))
                array_push($return, '<li><a href="#'.route('admin-managers-groups-delete').'?id='.$row->id. '" data-callback="managersGroups::ReloadTable" data-confirm="Are you sure you want to delete the item with the ID ' . $row->id .'?"> <i class="icon-remove"></i> Delete</a></li>');
            array_push($return,
                '</ul>
					</li>
				</ul>');
            return implode('', $return);
        })
        ->make(true);
    }

    public function edit(Request $request){
        $input = $request->only('id');
        $view = \View::make('core::managersGroups.ManagersGroupsForm');

        if(isset($input['id']) && !empty($input['id'])) {
            $group = ManagerGroup::find((int)$input['id']);

            if($group === null) {
                return Notify::error( trans('lrx::messages.2.2.5') , []);
            }

            $PermissionList = [];

            $permissions = \DB::table('managers_groups_permissions_relations')->where('group_id',$group->id)->get();

            if(count($permissions) > 0) {
                foreach($permissions as $value){
                    $PermissionList[ $value->permission_id ] = true;
                }
            }

            $view->with('groupName', $group->name)->with('id', $group->id )->with('PermissionList',$PermissionList);
        } else {
            $view->with('groupName', '')->with('id', '')->with('PermissionList', [] );
        }

        return \Response::json([
            'modal'=>[
                'title' => (isset($input['id'])) ? 'Edit' : 'Create',
                'content' => $view->render(),
                'cancel' => true,
                'submit' => (isset($input['id'])) ? false : true,
                'edit' => (isset($input['id'])) ? true : false,
            ]
        ]);

    }

    public function delete(Request $request) {
        $groupID = $request->only('id');

        if($groupID === 1) {
            return Notify::error( \Lang::get('lrx::messages.2.2.10'), []);
        }

        $group = ManagerGroup::find( $groupID )->first();

        // Check if is isset user in database
        if ( $group === null) {
            return Notify::error( \Lang::get('lrx::messages.2.2.5'), []);
        }

        \DB::table('managers_groups_relations')->where('group_id', $groupID)->delete();
        \DB::table('managers_groups_permissions_relations')->where('group_id', $groupID)->delete();

        if ( $group->delete() ){
            return  Notify::success(\Lang::get('lrx::messages.2.1.3') );
        } else {
            return Notify::error( \Lang::get('lrx::messages.2.2.7') );
        }
    }


    public function save(Request $request) {
        $input = $request->only(['ManagerGroupName', 'id', 'ManagerGroupsPermissionsList']);

        $validate_rules = [
            'ManagerGroupName' => 'required|alpha_dash|min:3|max:32',
            'ManagerGroupsPermissionsList' => 'required|array|min:1'
        ];

        $messages = [
            'ManagerGroupName.required' => trans('lrx::messages.2.2.11'),
            'ManagerGroupName.alpha_dash' => trans('lrx::messages.2.2.12'),
            'ManagerGroupName.min' => trans('lrx::messages.2.2.13'),
            'ManagerGroupName.max' => trans('lrx::messages.2.2.14'),
        ];

        $validate = \Validator::make($input, $validate_rules, $messages);

        // Check if input data is not valid
        if($validate->fails()) {
            $message = current($validate->messages()->toArray());
            return Notify::error($message[0], []);
        }

        if(empty($input['id'])) {
            if(ManagerGroup::where('name', $input['ManagerGroupName'])->count() > 0) {
                return Notify::error('Group with this name already exists.');
            }

            $group = ManagerGroup::create([
                'name' => $input['ManagerGroupName']
            ]);

            if(!$group) {
                return Notify::error('Something was wrong.');
            }

            foreach ($input['ManagerGroupsPermissionsList'] as $key=>$id) {
                \DB::table('managers_groups_permissions_relations')->insert([
                    'group_id' => $group->id,
                    'permission_id' => $key
                ]);
            }

            return Notify::success('New group was successfuly created.');
        } else {
            $group = ManagerGroup::where('id', $input['id'] )->first();

            if((ManagerGroup::where('name', $input['ManagerGroupName'])->count() > 0) && (bool)strcmp($input['ManagerGroupName'], $group->name)) {
                return Notify::error('Group with this name already exists.');
            }

            $group->name = $input['ManagerGroupName'];

            if(!$group->save()) {
                return Notify::error('Something was wrong.');
            }

            \DB::table('managers_groups_permissions_relations')->where('group_id', $group->id)->delete();

            foreach ($input['ManagerGroupsPermissionsList'] as $key=>$id) {
                \DB::table('managers_groups_permissions_relations')->insert([
                    'group_id' => $group->id,
                    'permission_id' => $key
                ]);
            }

            return Notify::success('New group was successfuly edited.');
        }
    }
}
