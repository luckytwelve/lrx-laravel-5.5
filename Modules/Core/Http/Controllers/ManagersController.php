<?php
namespace Modules\Core\Http\Controllers;

use DB;
use Hash;
use View;
use Validator;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Modules\Core\Helpers\Notify;
use Modules\Core\Entities\Manager;
use Illuminate\Routing\Controller;
use Modules\Core\Helpers\TableBuilder;
use Modules\Core\Entities\ManagerGroup;

class ManagersController extends Controller {
    public function lists() {
        $view  = view('core::managers.lists');
        $table = new TableBuilder('managers');
        $table->setButtons([
            sprintf(
                '<a href="#%s" class="pull-right btn btn-success">Create</a>',
                route('admin-managers-edit')
            )
        ])
        ->setColumns([
            [ 'title'=> trans('core::tables.id'), 'width'=> '5%' ],
            [ 'title'=> trans('core::tables.login'), 'width'=>'15%' ],
            [ 'title'=> trans('core::tables.email'), 'width'=>'15%' ],
            [ 'title'=> trans('core::tables.lastname'), 'width'=>'15%' ],
            [ 'title'=> trans('core::tables.firstname'), 'width'=>'15%' ],
            [ 'title'=> trans('core::tables.actions'), 'width'=>'10%' ]
        ])
        ->setName('Administration list');

        // Set breadcrumbs
        View::share('breadcrumbs', [
            [
                'name' => 'Home',
                'url' => route('admin')
            ],
            [
                'name' => 'Admins management',
                'url' => route('admin-managers-list')
            ]
        ]);

        return $view->with('table', $table->build());
    }

    public function listsJson() {
        $query = \DB::table('managers')->select([
            'managers.id as id',
            'managers.login as login',
            'managers.email as email',
            'managers.last_name as last_name',
            'managers.first_name as first_name'
        ]);

        return Datatables::of($query)
            ->addColumn('actions', function ($row) {
                return view('core::managers._actions')->with('row', $row);
            })
        ->make(true);
    }

    public function edit( Request $request ) {
        $input = $request->only('id');
        $groups = ManagerGroup::all();
        $view   = view('core::managers.form')->with([
            'Groups' => $groups
        ]);

        if(isset($input['id'])) {
            $Manager = Manager::find($input['id']);
            $Relations = $Manager->groups()->get();

            if(!$Manager) {
                return Notify::error('Manager not found.');
            }

            $view->with([
                'Manager' => $Manager,
                'Relations' => $Relations
            ]);
        }

        return response()->json([
            'modal'=>[
                'title' => (isset($input['id'])) ? 'Edit admin' : 'Create new admin',
                'content' => $view->render(),
                'cancel' => true,
                'submit' => (isset($input['id'])) ? false : true,
                'edit' => (isset($input['id'])) ? true : false,
            ]
        ]);
    }

    public function delete( Request $request ) {
        $input = $request->only('id');

        $validate = Validator::make($input, [
            'id' => 'required|integer|not_in:1|exists:managers,id'
        ]);

        if($validate->fails()) {
            $message = current($validate->messages()->toArray());

            return Notify::error($message[0]);
        }

        $manager = Manager::find((int)$input['id']);
        $manager->groups()->sync([]);

        if(!$manager->delete()) {
            return Notify::error('Something went wrong.');
        }

        return Notify::success('Manager was successfuly deleted.');
    }

    public function save(Request $request)
    {
        $input = $request->only([
            'id', 'login', 'email', 'first_name', 'last_name',
            'password', 'repassword', 'groups'
        ]);

        return (isset($input['id'])) ? $this->update($input) : $this->create($input);
    }

    private function create($input) {
        $validate = Validator::make($input, [
            'login'      => 'required|min:3|max:16|unique:managers,login',
            'email'      => 'required|min:5|max:128|unique:managers,email',
            'first_name' => 'required|min:2|max:32',
            'last_name'  => 'required|min:2|max:32',
            'password'   => 'required|min:6|max:128|same:repassword',
            'repassword' => 'required|min:6|max:128|same:password',
            'groups'     => 'required|array|min:1'
        ]);

        // Check if input data is not valid
        if($validate->fails()) {
            return Notify::error(
                current( $validate->messages()->toArray() )[0]
            );
        }

        $groups = ManagerGroup::whereIn('id', array_keys($input['groups']));

        if(count($input['groups']) != $groups->count()) {
            return Notify::error('One or many of selected groups are not found.');
        }

        $Manager = Manager::create([
            'login' => $input['login'],
            'email' => $input['email'],
            'password' => \Hash::make($input['password']),
            'last_name' => $input['last_name'],
            'first_name' => $input['first_name']
        ]);

        if(!$Manager) {
            return Notify::error('Something went wrong.');
        }

        $Manager->groups()->attach(array_keys($input['groups']));

        return Notify::success('Admin has been created.');
    }

    private function update($input) {
        $validate = Validator::make($input, [
            'login'      => [
                'required',
                'min:3',
                'max:16',
                Rule::unique('managers')->ignore($input['id'])
            ],
            'email'      => [
                'required',
                'min:5',
                'max:128',
                Rule::unique('managers')->ignore($input['id'])
            ],
            'first_name' => [ 'required', 'min:2', 'max:32' ],
            'last_name'  => [ 'required', 'min:2', 'max:32' ],
            'groups'     => [ 'required', 'array', 'min:1' ]
        ]);

        if($validate->fails()) {
            return Notify::error(
                current( $validate->messages()->toArray() )[0]
            );
        }

        $Manager = Manager::find((int) $input['id']);

        if(!$Manager) {
            return Notify::error('Admin not found');
        }

        if(!empty($input['password']) || !empty($input['repassword'])) {
            $validate = Validator::make($input, [
                'password'   => [
                    'required',
                    'min:6',
                    'max:128',
                    'same:repassword'
                ],
                'repassword' => [
                    'required',
                    'min:6',
                    'max:128',
                    'same:password'
                ]
            ]);

            if($validate->fails()) {
                return Notify::error(
                    current( $validate->messages()->toArray() )[0]
                );
            }

            $Manager->password = Hash::make($input['password']);
        }

        $groups = ManagerGroup::whereIn('id', array_keys($input['groups']));

        if(count($input['groups']) != $groups->count()) {
            return Notify::error('One or many of selected groups are not found.');
        }

        $Manager->login = $input['login'];
        $Manager->email = $input['email'];
        $Manager->last_name = $input['last_name'];
        $Manager->first_name = $input['first_name'];


        if(!$Manager->save()) {
            return Notify::error('Something went wrong.');
        }

        $Manager->groups()->sync(array_keys($input['groups']));

        return Notify::success('Admin have been edited.');
    }
}
