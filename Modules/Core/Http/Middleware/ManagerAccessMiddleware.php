<?php

namespace Modules\Core\Http\Middleware;

use Closure;
use Modules\Core\Helpers\Permission;

class ManagerAccessMiddleware {
    public function handle($request, Closure $next, $permission) {
        if(!Permission::managerHasPermission($permission)) {
            if($request->ajax()) {
                return response()->json([
                    'success' => false,
                    'message' => 'Forbidden'
                ]);
            } else {
                return abort(403);
            }
        }

        return $next($request);
    }
}
