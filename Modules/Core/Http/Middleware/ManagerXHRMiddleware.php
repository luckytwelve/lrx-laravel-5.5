<?php

namespace Modules\Core\Http\Middleware;

use Closure;

class ManagerXHRMiddleware {
    public function handle($request, Closure $next) {

//        $locale = $request->cookie('lang', \Config::get('app.locale'));
//        $locale =  \Config::get('app.locale');
//        \App::setLocale($locale);

        if(!\Request::ajax() && \Auth::guard('manager')->check()) {
            $Manager = \Auth::guard('manager')->user();
            $Group = ($Manager->groups()->count() > 0) ? $Manager->groups()->first() : false;

            die( \View::make('core::index.index', ['manager' => $Manager, 'manager_group' => $Group])->render() );
        }

        return $next($request);
    }
}
