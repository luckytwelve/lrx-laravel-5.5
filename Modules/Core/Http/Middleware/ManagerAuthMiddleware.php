<?php

namespace Modules\Core\Http\Middleware;

use Closure;

class ManagerAuthMiddleware {
    public function handle($request, Closure $next) {

        if(!\Auth::guard('manager')->check()) {
            if(\Request::ajax()) {
				return abort(401);
			} else {
				return \Redirect::away('/admin/account/login');
			}
        }

        session(['useKCFINDER' => true]);

        return $next($request);
    }
}
