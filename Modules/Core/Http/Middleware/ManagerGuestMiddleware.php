<?php

namespace Modules\Core\Http\Middleware;

use Closure;

class ManagerGuestMiddleware {
    public function handle($request, Closure $next) {
        if( \Auth::guard('manager')->check() ) {
			return \Redirect::away('/admin');
		}
        return $next($request);
    }
}
