<?php
Route::group(['prefix' => 'admin', 'namespace' => '\Modules\Core\Http\Controllers'], function () {
    Route::group(['middleware' => 'manager.guest'], function () {
        Route::get('/account/login', ['as' => 'admin-account-login', 'uses' => 'AccountController@login']);
        Route::post('/account/login', 'AccountController@loginJson');
    });
    Route::group(['middleware' => ['manager.auth', 'manager.xhr']], function () {
        Route::any('/', ['as' => 'admin', 'uses' => 'DashboardController@index']);

        Route::group(['prefix' => 'managers'], function() {
            Route::any('/lists', ['as'=>'admin-managers-list', 'uses'=>'ManagersController@lists', 'middleware' => 'manager.permission:managers-view']);
            Route::post('/listsJson', ['as'=>'admin-managers-listJson', 'uses'=>'ManagersController@listsJson', 'middleware' => 'manager.permission:managers-view']);
            Route::post('/delete', ['as'=>'admin-managers-delete', 'uses'=>'ManagersController@delete']);
            Route::post('/save', ['as'=>'admin-managers-save', 'uses'=>'ManagersController@save']);
            Route::post('/edit', ['as'=>'admin-managers-edit', 'uses'=>'ManagersController@edit']);
        });

        Route::group(['prefix' => 'managers-groups'], function() {
            Route::any('/lists', ['as'=>'admin-managers-groups-list', 'uses'=>'ManagersGroupsController@lists', 'middleware' => 'manager.permission:managers-groups-view']);
            Route::post('/listsJson', ['as'=>'admin-managers-groups-listJson', 'uses'=>'ManagersGroupsController@listsJson', 'middleware' => 'manager.permission:managers-groups-view']);
            Route::post('/edit', ['as'=>'admin-managers-groups-edit', 'uses'=>'ManagersGroupsController@edit']);
            Route::post('/delete', ['as'=>'admin-managers-groups-delete', 'uses'=>'ManagersGroupsController@delete']);
            Route::post('/save', ['as'=>'admin-managers-groups-save', 'uses'=>'ManagersGroupsController@save']);
        });
    });
    Route::any('/account/logout', ['as' => 'admin-account-logout', 'uses' => 'AccountController@logout']);
});
