ADM.modules.set('account_login', {
    ADMcontroller: 'login',
    events: {
        'submit: #loginForm': 'login',
    },

    init: function () {
        $(function() {
	       // Style checkboxes and radios
	       $('.styled').uniform();
       });
    },

    login: function (e, module) {
        e.preventDefault();
        $('button[type=submit]', this).prop('disabled', true);
        _this = this;


        $.ajax({
            type: 'post',
            dataType: 'json',
            cache: false,
            url: ADM.managerPath + '/account/login',
            data: $(this).serialize(),
            success: function ( response ) {
                $('button[type=submit]', _this).prop('disabled', false);
                switch (response.notification.type) {
                    case 'success':
                        setTimeout(document.location.href = ADM.managerPath, 1000);
                        break;
                    case 'error':
                        ADM.Notifications.error({
                            text: response.notification.text
                        });
                        break;
                }
            },
            error: function () {
                $('button[type=submit]',_this).prop('disabled', false);
            }
        });

        return false;
    }
});
