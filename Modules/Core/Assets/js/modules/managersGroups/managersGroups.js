ADM.modules.set('managersGroups', {
    tableSelector: '#managersGroups',
    events: {},
    checkAllPermissions: function() {
        $('.checkbox :checkbox').each(function() {this.checked = true;});
        $.uniform.update();
    },
    AfterFormSend: function( response, module ){
        if ( !response.notification ) return;
        if (response.notification.type == 'success'){
            $('.modal').modal('hide');
            $(module.tableSelector).DataTable().ajax.reload();
        }
    },
    ReloadTable: function( response, module){
        $(module.tableSelector).DataTable().ajax.reload();
    },
    init: function () {
        var _this = this;
        this.table = $(this.tableSelector).DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: ADM.managerPath + '/managers-groups/listsJson',
                type: 'POST'
            },
            columns: [
                {data: 'id', name: 'managers_groups.id'},
                {data: 'name', name: 'managers_groups.name'},
                {data: 'actions', orderable: false, searchable: false}
            ],
            initComplete: function () {
                $('.dataTables_length select').select2({
                    minimumResultsForSearch: Infinity,
                    width: 'auto'
                });
            }
        });
    }
});
