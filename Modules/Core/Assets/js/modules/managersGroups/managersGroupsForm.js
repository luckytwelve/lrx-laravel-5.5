ADM.modules.set('managersGroupsForm', {
    events: {
        'click: #checkAllPermissions' : 'checkAllPermissions'
    },

    checkAllPermissions: function(e) {
        e.preventDefault();
        $('.checkbox :checkbox').each(function() {this.checked = true;});
        $.uniform.update();
    },

    init: function () {
        
    },
});
