ADM.modules.set('managersForm', {
    events: {
        'click: #checkAllGroups' : 'checkAllPermissions'
    },

    checkAllPermissions: function(e) {
        e.preventDefault();
        $('.checkbox :checkbox').each(function() {this.checked = true;});
        $.uniform.update();
    },

    init: function () {

    },
});
