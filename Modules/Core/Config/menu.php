<?php
return [
    'core'=>[
        'index' => 1,
        'text' => '<span>Users and Permissions</span><i class="icon-users"></i>',
        'a.href' => '#',
        'permissions' => [
            'managers-view',
            'managers-groups-view'
        ],
        'childs' => [
            [
                'index' => 1,
                'text' => '<i class="icon-vcard"></i><span>Administrators list</span>',
                'a.href' => function(){ return route('admin-managers-list'); },
                'permissions' => ['managers-view'],
            ],
            [
                'index' => 2,
                'text' => '<span>Administrators groups list</span><i class="icon-collaboration"></i>',
                'a.href' => function(){ return route('admin-managers-groups-list'); },
                'permissions' => ['managers-groups-view'],
            ]
        ]
    ]
];
