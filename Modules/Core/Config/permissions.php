<?php
return [
    'Administrators' => [
        [
            'index' => 1,
            'name' => 'managers-view',
            'description' => 'Administrators view'
        ],
        [
            'index' => 2,
            'name' => 'managers-create',
            'description' => 'Administrators create'
        ],
        [
            'index' => 3,
            'name' => 'managers-edit',
            'description' => 'Administrators edit'
        ],
        [
            'index' => 4,
            'name' => 'managers-delete',
            'description' => 'Administrators delete'
        ]
    ],

    'Administrators managing groups' => [
        [
            'index' => 1,
            'name' => 'managers-groups-view',
            'description' => 'Administrators group view'
        ],
        [
            'index' => 2,
            'name' => 'managers-groups-create',
            'description' => 'Administrators group create'
        ],
        [
            'index' => 3,
            'name' => 'managers-groups-edit',
            'description' => 'Administrators group edit'
        ],
        [
            'index' => 4,
            'name' => 'managers-groups-delete',
            'description' => 'Administrators group delete'
        ]
    ]
];
