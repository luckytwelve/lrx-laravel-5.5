<?php namespace Moduels\Core\Inputs;


class Checkbox {
    
    public static function handle($setting, $param){

        $array = [];
        if (count($setting['values'])) {

            foreach ($setting['values'] as $value) {
                if ( is_array( $param ) ) {
                    $checked = in_array( $value['value'] , $param) ? 'checked' : '';
                } else {
                    $checked = '';
                }
                $checkbox = '<label>
					<input 
					class="'.$setting['class'].'"
					type="checkbox" 
					name="'.$setting['name'].'[]" 
					value="'.$value['value'].'" 
					'.$checked.' > 
					'.$value['text'].'
					</label><br/>';
                array_push($array, $checkbox);
            }
        }
        return implode('', $array);
    }
}



?>