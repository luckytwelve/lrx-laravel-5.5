<?php namespace Modules\Core\Inputs;
use Modules\Settings\Entities\Settings as Settings;

class Textarea{

    public static function  handle($setting, $param){
        return  '<textarea 
                        style="width:'.$setting['width'].';height:'.$setting['height'].'"
						id="'.(empty($setting['id'])?'texarea-'.$setting['name']: $setting['id'] ).'"
						class="'.$setting['class'].'" 
						name="'.$setting['name'].'" 
						'.$setting['area-size'].'
						">'.trim(( empty( $param ) ? $setting['default'] : $param)).'</textarea>';
    }
}

?>