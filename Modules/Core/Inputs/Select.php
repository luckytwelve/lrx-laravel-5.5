<?php namespace Modules\Core\Inputs;

class Select {

    public static function handle($setting, $param){

    $options = [];

    if (count($setting['values'])) {

        foreach ($setting['values'] as $value) {

            if ( !is_array($value) ){
                $value = array( 'text'=> $value, 'value' => $value );
            }
            
            $selected =  $value['value'] == $param ? 'selected' : '';

            $options[] = '<option 
                        value="'.$value['value'].'" 
                        '.$selected.' > 
                        '.trans($value['text']).'
                        </option>';
        }
    }

    return $html = '<select class="'.$setting['class'].'" name="'.$setting['name'].'">'.implode('', $options).'</select>';
    }
}

?>