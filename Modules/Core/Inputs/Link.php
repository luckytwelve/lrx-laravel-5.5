<?php namespace Modules\Core\Inputs;

class Link {

    public static function handle($setting, $param){

        return '<input class="'.$setting['class'].'"
					id="'.$setting['filemanager-id'].'"
					title="'.$setting['text'].'"
					type="text" 
					name="'.$setting['name'].'" 
					value="'.trim(( empty( $param ) ? $setting['default'] : $param)).'">';
    }
}

?>