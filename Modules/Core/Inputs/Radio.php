<?php namespace Modules\Core\Inputs;

class Radio{

    public static function handle($setting, $param){

        $array = [];
        if (count($setting['values'])) {

            foreach ($setting['values'] as $value) {
                $checked =  $value['value'] == $param ? 'checked' : '';

                $checkbox = '<label>
					<input 
					class="'.$setting['class'].'"
					type="radio" 
					name="'.$setting['name'].'" 
					value="'.$value['value'].'" 
					'.$checked.' > 
					'.$value['text'].'
					</label><br/>';
                array_push($array, $checkbox);
            }
        }
        
        return implode('', $array);

    }
}




?>