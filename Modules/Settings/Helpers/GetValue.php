<?php
namespace Modules\Settings\Helpers;
use Modules\Settings\Entities\Settings as Settings;

class GetValue{
    public static function get( $name ){

        $value = Settings::where('name', $name)->first();
        
        if ( empty($value) ) return $value;
        $json = json_decode( $value->values );

        if ( json_last_error() == 0 ){
            return $json;
        } else {
            return $value->values;
        }
    }
}

?>