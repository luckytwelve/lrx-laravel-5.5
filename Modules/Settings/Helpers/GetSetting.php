<?php namespace Modules\Settings\Helpers;
class GetSetting
{
    public static function GetSetting ($name, $settings_db)
    {
        if ($settings_db->count() > 0) {
            $item = $settings_db->where('name', $name)
                ->first();
            return ($item) ? $item['values'] : '';
        } else {
            return '';
        }
    }
}