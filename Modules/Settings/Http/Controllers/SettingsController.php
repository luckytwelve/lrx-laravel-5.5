<?php

namespace Modules\Settings\Http\Controllers;

use View;
use Cache;
use Request;
use Carbon\Carbon;
use Modules\Core\Helpers\Notify;
use Illuminate\Routing\Controller;
use Modules\Settings\Entities\Settings;
use Illuminate\Support\Facades\Artisan;

class SettingsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function lists ()
    {
        $view = view('settings::lists');
        $Settings = (Cache::has('settings')) ? collect(json_decode(Cache::get('settings'), true)) : Settings::all();
        $view->with('Settings', config('settings'));
        $view->with('Settings_db', $Settings);

        return $view;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveConfig ()
    {
        $input = Request::all();

        foreach ($input as $key => $value) {
            $count = Settings::where('name', $key)->count();

            if (is_array($value)) {
                $value = json_encode($value);
            }

            if ($count != 0) {
                $setting = Settings::where('name', $key)->first();

                $setting->update(['name' => $key, 'values' => $value]);
            } else {
                Settings::create(['name' => $key, 'values' => $value]);
            }
        }
        $settings = Settings::all();

        Cache::forever('settings', json_encode($settings));

        return Notify::success('Settings saved');
    }
}
