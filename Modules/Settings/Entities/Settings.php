<?php namespace Modules\Settings\Entities;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model {
    protected $fillable = ['name', 'values'];
    protected $table = 'settings';
    public $timestamps = false;

}