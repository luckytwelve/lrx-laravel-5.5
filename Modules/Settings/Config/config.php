<?php
return [
    'General'     => [
        'index'  => 1,
        'text'   => 'General',
        'childs' => [
            'site_name'         => [
                'name'    => 'Site name',
                'class'   => 'styled form-control',
                'text'    => "Set site name",
                'type'    => 'text',
                'default' => 'Quantumlisting',
            ],
            'acount_confrim'    => [
                'name'    => 'acount_confrim',
                'class'   => 'styled form-control',
                'text'    => "Set the number of days after which account will be deleted if user won't confirm the account",
                'type'    => 'text',
                'default' => '',
            ],
            'acount_deleted'    => [
                'name'    => 'acount_deleted',
                'class'   => 'styled form-control',
                'text'    => 'Set the number of days after which account should be deleted from the system',
                'type'    => 'text',
                'default' => '',
            ],
            'free_trial_period' => [
                'name'  => 'free_trial_period',
                'class' => 'styled form-control',
                'text'  => 'Set free trial period (months)',
                'type'  => 'text',
            ],
            'price_ru' => [
                'name'  => 'price_ru',
                'class' => 'styled form-control',
                'text'  => 'Product price RUB',
                'type'  => 'text',
            ],
            'price_en' => [
                'name'  => 'price_en',
                'class' => 'styled form-control',
                'text'  => 'Product price GBP',
                'type'  => 'text',
            ],

        ],
    ],
    'Links'       => [
        'index'  => 2,
        'text'   => 'Links',
        'childs' => [
            'googleplus'   =>
                [
                    'name'    => 'googleplus',
                    'class'   => 'styled form-control',
                    'text'    => 'Google plus link',
                    'type'    => 'text',
                    'default' => 'https://plus.google.com/',
                ],
            'facebook'     =>
                [
                    'name'    => 'facebook',
                    'class'   => 'styled form-control',
                    'text'    => 'Facebook link',
                    'type'    => 'text',
                    'default' => 'http://facebook.com',
                    'script'  => '',
                ],
            'instagram'    =>
                [
                    'name'    => 'instagram',
                    'class'   => 'styled form-control',
                    'text'    => 'Instagram link',
                    'type'    => 'text',
                    'default' => 'http://instagram.com',
                    'script'  => '',
                ],
            'youtube'      =>
                [
                    'name'    => 'youtube',
                    'class'   => 'styled form-control',
                    'text'    => 'Youtube link',
                    'type'    => 'text',
                    'default' => 'http://youtube.com',
                    'script'  => '',
                ],
            'linkedin'     =>
                [
                    'name'    => 'linkedin',
                    'class'   => 'styled form-control',
                    'text'    => 'Linkedin link',
                    'type'    => 'text',
                    'default' => 'https://www.linkedin.com',
                    'script'  => '',
                ],
            'news_link'    =>
                [
                    'name'    => 'news_link',
                    'class'   => 'styled form-control',
                    'text'    => 'The news funnel',
                    'type'    => 'text',
                    'default' => 'https://www.thenewsfunnel.com/newsroom/scripts/42daeb06e6c27e536d07f9acddb8d1f9.js',
                ],
            'twitter'      =>
                [
                    'name'    => 'twitter',
                    'class'   => 'styled form-control',
                    'text'    => 'Twitter link',
                    'type'    => 'text',
                    'default' => 'https://twitter.com/QuantumListing',
                ],
            'loan_link'    => [
                'name'    => 'loan_link',
                'class'   => 'styled form-control',
                'text'    => 'Loan link',
                'type'    => 'text',
                'default' => 'https://www.stacksource.com/lenders',
            ],
            'android_link' => [
                'name'  => 'android_link',
                'class' => 'styled form-control',
                'text'  => 'Android App link',
                'type'  => 'text',
            ],
            'ios_link'     => [
                'name'  => 'ios_link',
                'class' => 'styled form-control',
                'text'  => 'iOS App link',
                'type'  => 'text',
            ],
        ],
    ],
    'Pay'         => [
        'index'  => 3,
        'text'   => 'PayPal',
        'childs' => [
            'client_id'  =>
                [
                    'name'  => 'client_id',
                    'class' => 'styled form-control',
                    'text'  => 'Client ID',
                    'type'  => 'text',
                ],
            'secret'     =>
                [
                    'name'  => 'secret',
                    'class' => 'styled form-control',
                    'text'  => 'Secret',
                    'type'  => 'text',
                ],
            'mode_debug' =>
                [
                    'name'   => 'mode_debug',
                    'class'  => 'form-control',
                    'text'   => 'Mode debug',
                    'type'   => 'select',
                    'values' => [
                        ['value' => 'mode_enable', 'text' => 'enable'],
                        ['value' => 'mode_disable', 'text' => 'disable'],
                    ],
                ],

        ],
    ],
    'Contacts'    => [
        'index'  => 4,
        'text'   => 'Contacts',
        'childs' => [
            'address'                    => [
                'name'    => 'address',
                'class'   => 'styled form-control',
                'text'    => 'Address',
                'type'    => 'text',
                'default' => 'Jeomark LLC, 4 West Red Oak Lane',
            ],
            'email_for_support'          => [
                'name'    => 'email_for_support',
                'class'   => 'styled form-control',
                'text'    => 'For support',
                'type'    => 'text',
                'default' => 'support@quantumlisting.com',
            ],
            'email_to_reach_our_founder' => [
                'name'    => 'email_to_reach_our_founder',
                'class'   => 'styled form-control',
                'text'    => 'To reach our Founder',
                'type'    => 'text',
                'default' => 'david@quantumlisting.com',
            ],
            'feedback_email'             =>
                [
                    'name'    => 'feedback_email',
                    'class'   => 'styled form-control',
                    'text'    => 'Feedback email',
                    'type'    => 'text',
                    'default' => 'alswell.dev@gmail.com',
                    'script'  => '',
                ],
            'call_us_phone'              => [
                'name'    => 'call_us_phone',
                'class'   => 'styled form-control',
                'text'    => 'Phone',
                'type'    => 'text',
                'default' => '914-470-5719',
            ],
            'fax'                        => [
                'name'    => 'fax',
                'class'   => 'styled form-control',
                'text'    => 'Fax',
                'type'    => 'text',
                'default' => '914-949-7074',
            ],
        ],
    ],
    'Maintenance' => [
        'index'  => 5,
        'text'   => 'Maintenance mode',
        'childs' => [
            'maintenance_title' => [
                'name'  => 'maintenance_title',
                'class' => 'styled form-control',
                'text'  => 'Set maintenance title',
                'type'  => 'text',
            ],
            'maintenance_desc'  => [
                'name'  => 'maintenance_desc',
                'class' => 'styled form-control',
                'text'  => 'Set maintenance desc ',
                'type'  => 'text',
            ],
            'maintenance_mode'  => [
                'name'   => 'maintenance_mode',
                'class'  => 'form-control',
                'text'   => 'Set maintenance mode',
                'type'   => 'select',
                'values' => [
                    ['value' => 'mode_enable', 'text' => 'enable'],
                    ['value' => 'mode_disable', 'text' => 'disable'],
                ],
            ],
            'maintenance_time'  => [
                'name'  => 'maintenance_time',
                'class' => 'form-control pickatime',
                'text'  => 'Set time',
                'type'  => 'text',
            ],
            'maintenance_date'  => [
                'name'  => 'maintenance_date',
                'class' => 'form-control datepicker',
                'text'  => 'Set date',
                'type'  => 'text',
            ],
        ],
    ],
    'Salesforce'  => [
        'index'  => 6,
        'text'   => 'Salesforce settings',
        'childs' => [
            'pardot_status'  => [
                'name'   => 'pardot_status',
                'class'  => 'form-control',
                'text'   => 'Status',
                'type'   => 'select',
                'values' => [
                    ['value' => 'enabled', 'text' => 'Enabled'],
                    ['value' => 'disabled', 'text' => 'Disabled'],
                ],
            ],
            'pardot_email' => [
                'name'  => 'pardot_email',
                'class' => 'styled form-control',
                'text'  => '<b>Pardot</b>.<br> The email address of the user account',
                'type'  => 'text',
            ],
            'pardot_key' => [
                'name'  => 'pardot_key',
                'class' => 'styled form-control',
                'text'  => '<b>Pardot</b>.<br> The user key of the user account<br>(in My Settings)',
                'type'  => 'text',
            ],
            'pardot_password' => [
                'name'  => 'pardot_password',
                'class' => 'styled form-control',
                'text'  => '<b>Pardot</b>.<br> The account password',
                'type'  => 'text',
            ],
        ],
    ],
    'Yandex'      => [
        'index'  => 7,
        'text'   =>  '<img src="'.url('/img/yandex_service.png').'">'.' Yandex settings',
        'childs' => [
            'yandex_test_mode'  => [
                'name'   => 'yandex_test_mode',
                'class'  => 'form-control',
                'text'   => '<b>Test mode</b>',
                'type'   => 'select',
                'values' => [
                    ['value' => '1', 'text' => 'Enabled'],
                    ['value' => '0', 'text' => 'Disabled'],
                ],
            ],
            'yandex_shopId' => [
                'name'  => 'yandex_shopId',
                'class' => 'styled form-control',
                'text'  => '<b>Shop ID</b>',
                'type'  => 'text',
            ],
            'yandex_scid' => [
                'name'  => 'yandex_scid',
                'class' => 'styled form-control',
                'text'  => '<b>Shop showcase ID</b>',
                'type'  => 'text',
            ],
            'yandex_return_success_url' => [
                'name'  => 'yandex_return_success_url',
                'class' => 'styled form-control',
                'text'  => '<b>Success URL</b><br> Return to url if payment is successful',
                'type'  => 'text',
            ],
            'yandex_return_cancel_url' => [
                'name'  => 'yandex_return_cancel_url',
                'class' => 'styled form-control',
                'text'  => '<b>Fail URL</b><br> Return to url if payment is fail',
                'type'  => 'text',
            ],
        ],
    ],
    'Checkout'    => [
        'index'  => 8,
        'text'   =>  '<img src="'.url('/img/checkout_service.png').'">'.' Checkout settings',
        'childs' => [
            'checkout_test_mode'  => [
                'name'   => 'checkout_test_mode',
                'class'  => 'form-control',
                'text'   => '<b>Test mode</b>',
                'type'   => 'select',
                'values' => [
                    ['value' => '1', 'text' => 'Enabled'],
                    ['value' => '0', 'text' => 'Disabled'],
                ],
            ],
            'checkout_secret_key' => [
                'name'  => 'checkout_secret_key',
                'class' => 'styled form-control',
                'text'  => '<b>Secret key</b>',
                'type'  => 'text',
            ],
            'checkout_public_key' => [
                'name'  => 'checkout_public_key',
                'class' => 'styled form-control',
                'text'  => '<b>Public key</b>',
                'type'  => 'text',
            ]
        ],
    ],
];
