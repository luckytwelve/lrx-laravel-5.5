<?php
return [
    'settings' =>[
        'index' => 8,
        'text' => '<span>Settings</span><i class="icon-cog3"></i>',
        'a.href' => function(){ return route('admin-settings-lists'); },
    ]
];
