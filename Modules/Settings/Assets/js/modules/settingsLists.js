ADM.modules.set('settingsLists', {
    events: {
        'click: #showTab a': 'changeTab',
    },
    changeTab: function(e, module) {
        $('[data-type=tab]').hide();
        $('#' + $(this).data('tab')).show();
        e.preventDefault();
    },
    initDatepicker: function() {
        $('.datepicker').pickadate({
            min: true,
            selectYears: true,
            selectMonths: true,
            formatSubmit: 'mm/dd/yyyy',
            format: 'mm/dd/yyyy'
        });
    },
    initTimepicker: function (){
        $('.pickatime').pickatime();
    },
    init: function () {
    	$('.tip').tooltip();
    	console.log('fired');
        this.initDatepicker();
        this.initTimepicker();
    }
});