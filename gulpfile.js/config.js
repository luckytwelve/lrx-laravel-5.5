var Config = {
    admin: {
        theme: {
            base_path: 'resources/assets/admin/theme',
            dest_path: 'public/assets/admin'
        },
        core: {
            base_path: 'resources/assets/admin/adm',
            dest_path: 'public/assets/admin'
        },
        components: {
            base_path: 'resources/assets/admin/components',
            dest_path: 'public/assets/admin'
        },
        modules: {
            base_path: 'Modules',
            dest_path: 'public/assets/admin'
        }
    },
    site: {
        base_path: 'resources/assets/site',
        dest_path: 'public/assets'
    },
    getTaskSettings: function (task_name) {
        return task_name.split(':').reduce(function (result, key) {
            return result[key];
        }, this.tasks);
    },
    tasks: {
        admin: {},
        network: {}
    }
};

/*
 * ADMIN:ADM TASKS SETTINGS
 */
Config.tasks.admin.core = {
    js: {
        source: '/**/*',
        dist: '/js',
        order: [
            'core/**/*',
            'core/adm.js',
            '**/*',
        ]
    }
};

/*
 * ADMIN PLUGINS COMPONENTS TASKS SETTINGS
 */
Config.tasks.admin.components = {
    js: {
        source: '/**/*',
        dist: '/js',
        order: [
            'libs/moment.min.js',
            'libs/picker.js',
            'libs/picker.date.js',
            'libs/picker.time.js',
            'libs/**/*',
            '*'
        ]
    }
};

/*
 * ADMIN:THEME TASKS SETTINGS
 */
Config.tasks.admin.theme = {
    assets: {
        source: '/assets/**/*',
        dist: '/'
    },
    css: {
        less: {
            source: '/css/less/*.less',
            dist: '/css'
        },
        extras: {
            source: '/css/extras/**/*.css',
            dist: '/css'
        }
    },
    js: {
        source: '/js/**/*',
        dist: '/js',
        order: [
            'libs/jquery.min.js',
            'libs/bootstrap.min.js',
            'theme.js',
            //'plugins/*',
            //'plugins_init.js'
        ]
    }
};

/*
 * ADMIN:ADM TASKS SETTINGS
 */
Config.tasks.admin.modules = {
    js: {
        source: '/**/Assets/js/modules/**/*.js',
        dist: '/js'
    }
};

Config.tasks.site = {
    assets: {
        source: '/assets/**/*',
        dist: '/'
    },
    css: {
        libs: {
            source: '/css/plugins/*.css',
            dist: '/css/'
        },
        sass: {
            source: '/css/sass/',
            dist: '/css/',
        },
        fonts: {
            source: '/css/fonts/**/*.{ttf,woff,woff2,eof,svg}',
            dist: '/css/fonts/',
        }
    },

    js: {
        libs: {
            source: '/js/libs/**/*.js',
            dist: '/js',
            order: [
                'jquery.js',
                '/**/*.js'
            ]
        },
        core: {
            source: '/js/core/**/*.js',
            dist: '/js',
            order: [
                'js/core/**/*',
                'js/core/app.js',
                'js/**/*',
            ]
        },
        modules: {
            source: '/js/modules/**/*.js',
            dist: '/js'
        }
    }
};

module.exports = Config;
