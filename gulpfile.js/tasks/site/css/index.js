var gulp = require('gulp');
var sass = require('gulp-sass');
var args = require('get-gulp-args')();
var cleanCSS = require('gulp-clean-css');
var concatCSS = require('gulp-concat-css');
var Config = require('../../../config.js');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');

var BASE_PATH = Config.site.base_path;
var DEST_PATH = Config.site.dest_path;

gulp.task('site:css:libs', function () {
    var settings = Config.getTaskSettings('site:css:libs');
    var dest = gulp.src(BASE_PATH + settings.source)
        .pipe(concatCSS('libs.css'))
        .pipe(cleanCSS('libs.css'));

    if(args.production != undefined) {
        dest = dest.pipe(
            cleanCSS({
                compatibility: 'ie8',
                specialComments: 0,
                keepSpecialComments: false
            })
        );
    }

    return dest.pipe(gulp.dest(DEST_PATH + settings.dist))
});

gulp.task('site:css:sass', function() {
    var settings = Config.getTaskSettings('site:css:sass');
    var dest = gulp.src(BASE_PATH + settings.source + 'style.sass')
        .pipe(sourcemaps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(autoprefixer('last 15 versions'));

    if(args.production != undefined) {
        dest = dest.pipe(
            cleanCSS({
                compatibility: 'ie8',
                specialComments: 0,
                keepSpecialComments: false
            })
        );
    }

    return dest.pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(DEST_PATH + settings.dist));
});

gulp.task('site:css:fonts', function() {
    var settings = Config.getTaskSettings('site:css:fonts');

    return gulp.src(BASE_PATH + settings.source)
        .pipe(
            gulp.dest(DEST_PATH + settings.dist)
        );
});

gulp.task('site:css', [
    'site:css:libs',
    'site:css:sass',
    'site:css:fonts'
], function() {

});
