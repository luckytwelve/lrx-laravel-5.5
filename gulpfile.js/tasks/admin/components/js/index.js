var Config = require('../../../../config.js');
var BASE_PATH = Config.admin.components.base_path;
var DEST_PATH = Config.admin.components.dest_path;

var gulp = require('gulp');
var concat = require('gulp-concat');
var order = require('gulp-order');
var uglify = require('gulp-uglify');

gulp.task('admin:components:js', function()
{
	var settings = Config.getTaskSettings('admin:components:js');

	return gulp.src(BASE_PATH + settings.source)
		.pipe(order(settings.order))
		.pipe(uglify())
		.pipe(concat('components.min.js'))
		.pipe(gulp.dest(DEST_PATH + settings.dist));
});
