var Config = require('../../../../config.js');
var BASE_PATH = Config.admin.core.base_path;
var DEST_PATH = Config.admin.core.dest_path;

var gulp = require('gulp');
var concat = require('gulp-concat');
var order = require('gulp-order');
var uglify = require('gulp-uglify');

gulp.task('admin:core:js', function()
{
	var settings = Config.getTaskSettings('admin:core:js');

	return gulp.src(BASE_PATH + settings.source)
		.pipe(order(settings.order))
		.pipe(uglify())
		.pipe(concat('core.min.js'))
		.pipe(gulp.dest(DEST_PATH + settings.dist));
});
