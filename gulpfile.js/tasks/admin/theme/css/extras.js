var Config = require('../../../../config.js');
var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');

var BASE_PATH = Config.admin.theme.base_path;
var DEST_PATH = Config.admin.theme.dest_path;

gulp.task('admin:theme:css:extras', function()
{
	var settings = Config.getTaskSettings('admin:theme:css:extras');

	return gulp.src(BASE_PATH + settings.source)
		.pipe(concat('extras.min.css'))
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(gulp.dest(DEST_PATH + settings.dist));
});
