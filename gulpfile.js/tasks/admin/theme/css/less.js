var Config = require('../../../../config.js');
var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var less = require('gulp-less');

var BASE_PATH = Config.admin.theme.base_path;
var DEST_PATH = Config.admin.theme.dest_path;

gulp.task('admin:theme:css:less', function()
{
	var settings = Config.getTaskSettings('admin:theme:css:less');

	return gulp.src(BASE_PATH + settings.source)
		.pipe(less())
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(rename({suffix: ".min"}))
		.pipe(gulp.dest(DEST_PATH + settings.dist));
});
