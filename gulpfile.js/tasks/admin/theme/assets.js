var Config = require('../../../config.js');
var BASE_PATH = Config.admin.theme.base_path;
var DEST_PATH = Config.admin.theme.dest_path;

var gulp = require('gulp');

gulp.task('admin:theme:assets', function()
{
	var settings = Config.getTaskSettings('admin:theme:assets');

	return gulp.src(BASE_PATH + settings.source)
		.pipe(gulp.dest(DEST_PATH + settings.dist));
});
