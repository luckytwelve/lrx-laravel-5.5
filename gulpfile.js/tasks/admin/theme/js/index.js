var Config = require('../../../../config.js');

var gulp = require('gulp');
var concat = require('gulp-concat');
var order = require('gulp-order');
var uglify = require('gulp-uglify');

var BASE_PATH = Config.admin.theme.base_path;
var DEST_PATH = Config.admin.theme.dest_path;

gulp.task('admin:theme:js', function() {
	var settings = Config.getTaskSettings('admin:theme:js');

	return gulp.src(BASE_PATH + settings.source)
		.pipe(order(settings.order))
		.pipe(uglify())
		.pipe(concat('theme.min.js'))
		.pipe(gulp.dest(DEST_PATH + settings.dist));
});
