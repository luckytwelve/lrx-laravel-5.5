var Config = require('../../../../config.js');
var BASE_PATH = Config.admin.modules.base_path;
var DEST_PATH = Config.admin.modules.dest_path;

var gulp = require('gulp');
var concat = require('gulp-concat');
var order = require('gulp-order');
var uglify = require('gulp-uglify');

gulp.task('admin:modules:js', function()
{
	var settings = Config.getTaskSettings('admin:modules:js');

	return gulp.src(BASE_PATH + settings.source)
		.pipe(order(settings.order))
		.pipe(uglify())
		.pipe(concat('modules.min.js'))
		.pipe(gulp.dest(DEST_PATH + settings.dist));
});
