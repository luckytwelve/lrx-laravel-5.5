var Config = require('../../../config.js');
var BASE_PATH = Config.admin.modules.base_path;
var DEST_PATH = Config.admin.modules.dest_path;

var gulp = require('gulp');
var args = require('get-gulp-args')();

gulp.task('admin:modules', ['admin:modules:js'], function() {
	if (args.production == undefined) {
		var settings = Config.getTaskSettings('admin:modules:js');
		gulp.watch(BASE_PATH + settings.source, ['admin:modules:js']);
  }
});
