var gulp = require('gulp');

gulp.task('admin', [
	'admin:theme',
	'admin:components',
	'admin:core',
	'admin:modules'
]);
