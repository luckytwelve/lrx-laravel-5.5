<?php

use Illuminate\Database\Seeder;
use Modules\Core\Entities\ManagerGroup;

class ManagersGroupsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ManagerGroup::create([
            'name' => 'Administrator'
        ]);
    }
}
