<?php

use Illuminate\Database\Seeder;
use Modules\Core\Entities\Manager;

class ManagersDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Manager::create([
            'login' => 'manager',
            'email' => 'admin@admin.com',
            'password' => \Hash::make('123qqq'),
            'last_name' => 'Admin',
            'first_name' => 'Admin'
        ]);

        \DB::table('managers_groups_relations')->insert([
            'manager_id' => 1,
            'group_id' => 1
        ]);
    }
}
