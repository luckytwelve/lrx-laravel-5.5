<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('managers', function(Blueprint $table) {
            $table->increments('id');
            $table->string('login', 32)->unique();
            $table->string('email', 255)->unique();
            $table->string('password', 60);
            $table->rememberToken();
            $table->string('last_name', 128);
            $table->string('first_name', 128);
            $table->Integer('created_at');
            $table->Integer('updated_at');
            $table->Integer('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('managers');
    }
}
