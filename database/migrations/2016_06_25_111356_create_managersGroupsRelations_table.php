<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagersGroupsRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('managers_groups_relations', function(Blueprint $table) {
           $table->increments('id');
           $table->Integer('manager_id')->index();
           $table->Integer('group_id')->index();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('managers_groups_relations');
    }
}
