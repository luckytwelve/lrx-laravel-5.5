<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagersGroupsPermissionsRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('managers_groups_permissions_relations', function($table) {
            $table->increments('id');
            $table->integer('group_id')->index();
            $table->string('permission_id', 255)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('managers_groups_permissions_relations');
    }
}
