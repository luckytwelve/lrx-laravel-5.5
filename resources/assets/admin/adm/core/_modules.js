/**
*
* @type {{name: string, el: {}, _init: Function, init: Function, events: Function, _parseCommand: Function, _eventsDestroy: Function, _eventInit: Function, destroy: Function}}
* @private
*/

var _Module = {
    name:'',
    el: {},
    _init: function ( debug ) {
       if(debug === undefined) debug = false;
        this.destroy();
        if (this.init) this.init( debug );
        if (this.events) this._eventInit( debug );
        this.el().attr('data-instance',this);
    },
    init: function ( debug ) {
    },
    events: function () {
    },
    _parseCommand: function( command ){
        var command = command.split(':');
        var event = command[0];
        command.splice(0,1);
        var selector = command.join(':');
        return {event:event, selector:selector};
    },
    _eventsDestroy: function(){
        var module = this;
        for(var key in this.events){
            command = this._parseCommand( key );
            $(document).off(command.event, '[data-module='+module.name+'] ' +command.selector);
        }
    },
    _eventInit: function( debug ){
        var module = this;
        for(var key in this.events){
            command = this._parseCommand( key );
            if ( debug ) console.log('Model: '+ module.name + '  Attach event "'+command.event+'" on: [data-module='+module.name+']' + command.selector    );

            (function($,command, key, module){
                var _function = module.events[key] === 'function'? module.events[key] : module[module.events[key]] ;
                $(document).on(command.event, '[data-module='+module.name+'] ' +command.selector, function(e){
                    var result = _function.apply(this, [e,module]) ;
                    return result;
                });
            })(jQuery, command, key, module)

        }
    },

    destroy: function(){
        var module = this;
        for(var key in this.events){
            command = this._parseCommand( key );
            $(document).off(command.event, '[data-module='+module.name+'] ' +command.selector);
        }
    }
};

var _Modules = {
    modules: {},
    set: function (name, module) {
        module.name = name;
        module.el = function(){
            return $('[data-module='+name+']');
        }
        this.modules[name] = $.extend({}, _Module, module);  //_module.extend( module )
    },
    get: function (name) {
        return this.modules[name];
    },
    init: function () {
        $("[data-module]").each(function(){
            var modul = $(this).attr('data-module');
            if ( !$(this).attr('data-instance') && ADM.modules.modules[modul] ) {
                ADM.modules.get(modul)._init();
            }
        });
    }
};
