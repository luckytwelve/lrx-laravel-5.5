/**
*
* @type {{name: string, el: {}, _init: Function, init: Function, events: Function, _parseCommand: Function, _eventsDestroy: Function, _eventInit: Function, destroy: Function}}
* @private
*/

var _UIcomponent = {
	name: '',
	context: 'body', 
	init: function(context) {
		
	},
	destroy: function(context) {
		
	}
}

var _UI = {
	components: {},
    set: function (name, component) {
    	component.name = name;
    	this.components[name] = $.extend({}, _UIcomponent, component);
    },
    get: function (name) {
        return this.components[name];
    },
    init: function (context) {
		for(var name in this.components) {
			var init_context = (context) ? context : this.components[name].context;
			this.components[name].init(init_context);
		}
    },
    destroy: function (context) {
		for(var name in this.components) {
			var destroy_context = (context) ? context : this.components[name].context;
			this.components[name].destroy(destroy_context);
		}
    }
};

