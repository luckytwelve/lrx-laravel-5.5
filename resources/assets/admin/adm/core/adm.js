ADM = {
    setting: {
        lang: 'ru',
    },
    Notifications: _Notifications,
    template: _Template,
    ajax: _Ajax,
    modules: _Modules,
    modal: _Modal,
    ui: _UI,
    //@TODO: Deprecated
    showViewport: function (viewName) {
        ADM.template.load(viewName, function (response) {
            var obj = $.extend({}, ADM.setting, response);
            $('#viewport').html(ADM.template.parse(response, obj));
        });
    },
    setViewPort: function (html) {
        $('#viewport').html(ADM.template.parse(response, ADM.setting));
    },

    RequestParse: function (request) {
        if (request.notification) ADM.Notifications.message(request.notification);
        if (request.responseText)    {
			$('#viewport').html(request.responseText);
            ADM.ui.init( $('#viewport') );
        }
        if (request.modal)   ADM.modal.show(request.modal);

        ADM.modules.init();
    },

    init: function () {
        ADM.ajax({url: location.href});

        $(window).on('popstate',function(event) {
            ADM.ajax({
                url: location.href,
                success: $(this).attr('data-callback')
            });
        });

        window.onload = function(e) {
			var hash = location.hash;
			if(hash == '' || hash == '#') return;
            ADM.ajax({ url: hash.substring(1) });
        };

        $(document).on('click', 'a', function (e) {
            var confirmData = $(this).attr('data-confirm');

            if(confirmData) {
                var isConfirmed = confirm(confirmData);
                if(!isConfirmed) return false;
            }

            var href = $(this).attr('href');

            if ( !href) return;
            if ( $(this).data('toggle') == 'tab' ) return;
            if ( $(this).hasClass('no-ajax') ) return;
            if ( href === '#') return;
            if ( $(this).attr('target') ) return;
            if ( $(this).closest('.cke_reset').length > 0 ) return;
            if ( $(this).closest('.cke_reset_all').length > 0 ) return;

            e.preventDefault();

            if(CKEDITOR != undefined) {
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
            }

            if (href.indexOf('#') != 0) {
                history.pushState({}, $(this).attr('title'), href);
            } else {
                href = href.substring(1);
            }

            ADM.ajax({
                url: href,
                success: $(this).attr('data-callback')
            });
        });

        $(document).on('submit', 'form', function () {
            var form = $(this);
            action = form.attr('action');

            if (!action) return;
            if (action === '#') return;

            if(CKEDITOR != undefined) {
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
            }

            ADM.ajax({
                url: action,
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: $(this).attr('data-callback')
            });
            return false;
        });
    }
};

$(function() {
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,

        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            //$(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            //$(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });
});
