_Notifications = {
    defaults: {
        title: '',
        text: '',
        addclass: 'bg-primary',
        delay: 3000,
        type: 'info'
    },
    success: function (params) {
        params = params || {};
        params.addclass = 'bg-success';
        this.show(params);
    },
    info: function (params) {
        params = params || {};
        params.addclass = 'bg-info';
        this.show(params);
    },
    warning: function (params) {
        params = params || {};
        params.addclass = 'bg-warning';
        this.show(params);
    },
    error: function (params) {
        params = params || {};
        params.addclass = 'bg-danger';
        this.show(params);
    },
    show: function(params) {
        new PNotify(params);
    },
    message: function (params) {
        params = params || {};
        params = $.extend({}, this.defaults, params);
        this[params.type](params);
    }
}
