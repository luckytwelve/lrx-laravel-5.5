/**
 *
 * @type {{show: Function, hide: Function}}
 * @private
 *
 * V0.2 chnge log:
 * - 1.05.2015 Add multiDialogs support
 */
_Modal = {
    instance: {},
    close: function( name ){
        if(typeof(name)==='undefined') {
            this.hide();
        } else {
            if ( !this.instance[ name ] ) {
                console.log( 'Dialog "' + name + '" not found.');
                return;
            }
            this.instance[ name ].close();
            delete this.instance[ name ];
        }
    },
    show: function( params ){
        var modal = $('#modal');
        var buttons = [];

        if ( params.cancel ) {
            buttons.push({
                label: "Cancel",
                action: function(dialog) {
                    dialog.close();
                },
                cssClass: 'modal-cancel btn btn-warning'
            });
        }
        if ( params.submit ) {
            buttons.push({
                label: "Save",
                action: function(dialog) {
                    //dialog.closest('.modal-content').find('form').submit();
                    dialog.getModal().find('form').submit();
                },
                cssClass: 'modal-submit btn btn-primary'
            });
        }

        if ( params.edit ) {
            buttons.push({
                label: "Edit",
                action: function(dialog) {
                    dialog.getModal().find('form').submit();
                },
                cssClass: 'modal-edit btn btn-primary'
            });
        }

        var onshownCallback = params.onshown || function(){};


        params.onshown = function( dialog ){
            ADM.modules.init();
            ADM.ui.init( dialog.$modal[0] );

            eval( ' _onshownCallback = ' + onshownCallback );
            _onshownCallback( dialog.$modal[0] );
        }

        var onhideCallback = params.onhide ? params.onhide : function(){};
        params.onhide = function( dialog ){
			ADM.ui.destroy( dialog.$modal[0] );

            eval( ' _onshownCallback = ' + onshownCallback );
            _onshownCallback( dialog.$modal[0] );
        }
        if ( !params.buttons) {
            params.buttons = buttons;
        } else {
            for(var i in params.buttons){
              if ( params.buttons[i].action ) {
                var func = new Function( 'return function(dialog) {' + params.buttons[i].action + '}' );
                params.buttons[i].action = func();
              }
              buttons.push( params.buttons[i] );
            }
            params.buttons = buttons;
        }
        params.message = params.content;
        params.nl2br = false;

        modal = BootstrapDialog.show(params);
        var width = params.width || 900;
        modal.$modalDialog.css({width: (width > $(window).width() ? $(window).width() - 35: width ) , height: params.height || 500});

        if (params.name ){
            this.instance[ params.name ] = modal;
        }
        return modal;
    },
    hide: function( modal ){
        if ( modal ) {
            modal.close();
        } else {
            BootstrapDialog.closeAll()
        }
    },
    /*
    * Replace modal content
    */
    replace: function(name, html) {
         if(typeof(name)==='undefined') return;
         this.instance[ name ].getModal().find('form .well').html(html).promise().done(function() {

         });
    }
};
