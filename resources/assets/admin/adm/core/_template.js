/**
 *
 * @type {{get: Function, parse: Function, load: Function}}
 * @private
 */
_Template = {
    //facade
    get: function (html) {
        return _.template(html)
    },
    //facade
    parse: function (template, obj) {
        var tpl = ADM.template.get(template);
        return tpl(obj);
    },

    load: function (name, callback) {
        $.ajax({
            url: name,
            type: 'post',
            dataType: 'html',
            cache: false,
            success: function (response, codeMessage, xhr) {
                callback(response);
            },
            error: function (response) {
                ADM.Notifications.warning("Something was wrong.");
            }
        });
    }
};
