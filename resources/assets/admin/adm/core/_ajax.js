/*
    AJAX default settings
*/
$.ajaxSetup({
    statusCode: {
       401: function() {
           window.location.href = ADM.managerPath + '/account/login';
       },
       403: function() {
           ADM.Notifications.error({text: "You are not have permissions for this action."});
       },
       404: function() {
           ADM.Notifications.error({text: "Page not found."});
       }
   },
   error: function() {
       ADM.Notifications.error({text: "Something was wrong."});
   }
});

_Ajax = function (params) {
    var _success = params.success;
    delete params.success;

    $('.loader').fadeIn( 300 );
    params = $.extend(true, {}, {
        type: 'post',
        dataType: 'json',
        cache: false,
        url: '',
        error: function ( request ) {
            $('.loader').fadeOut( 300 );
            if(parseInt(request.status) == 401) {
                window.location.href=ADM.managerPath + '/account/login';
            } else {
                ADM.RequestParse( request );
            }
        },
        success: function ( request, codeMessage, xhr) {
            $('.loader').stop( true, true).fadeOut( 500 );
                ADM.RequestParse( request );
                if (_success) {
                    if ( typeof _success === 'function' ){
                        _success( request );
                    } else if ( _success.indexOf('::') ){
                        _tmp = _success.split('::');
                        _success = ADM.modules.get( _tmp[0] )[_tmp[1]];
                        _success( request,  ADM.modules.get( _tmp[0] ));
                    }
                }

        }
    }, params);
    $.ajax(params);
};
