/* ------------------------------------------------------------------------------
*
*  # Basic form inputs
*
*  Specific JS code additions for form_input_basic.html page
*
*  Version: 1.0
*  Latest update: Aug 1, 2015
*
* ---------------------------------------------------------------------------- */

ADM.ui.set('ckeditor', {
	init: function(context) {
		var editors = $(context).find('[data-ui="ckeditor"]');

		if(editors.length > 0) {
			$.each(editors, function(index, editor) {
				CKEDITOR.replace( $(editor).attr('name') ,{
					filebrowserBrowseUrl : '/fs/dialog.php?type=2&editor=ckeditor&fldr=',
					filebrowserUploadUrl : '/fs/dialog.php?type=2&editor=ckeditor&fldr=',
					filebrowserImageBrowseUrl : '/fs/dialog.php?type=1&editor=ckeditor&fldr='
				});
			});
		}
	}
});
