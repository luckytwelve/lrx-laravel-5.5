/* ------------------------------------------------------------------------------
*
*  # Basic form inputs
*
*  Specific JS code additions for form_input_basic.html page
*
*  Version: 1.0
*  Latest update: Aug 1, 2015
*
* ---------------------------------------------------------------------------- */

ADM.ui.set('component_name', {
	init: function(context) {

		$(".styled, .multiselect-container input", context).uniform({
			radioClass: 'choice'
		});

		// File input
		$(".file-styled", context).uniform({
			wrapperClass: 'bg-blue',
			fileButtonHtml: '<i class="icon-file-plus"></i>'
		});

	}
});

