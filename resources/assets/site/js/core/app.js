var App = {
    ajax: _Ajax,
    modules: _Modules,
    notification: _Notification,
    geolocation: _Geolocation,
    cookies: _Cookies,
    lang: _Lang,
    RequestParse: function (response) {
        if(typeof response.success !== 'undefined') {
            if(response.message != undefined) {
                if(response.success) {
                    App.notification.success(response.message);
                } else {
                    App.notification.error(response.message);
                }
            }
        }
        if (response.redirect) window.location = response.redirect;

        App.modules.init();
    },
    isOnPage: function (selector) {
        return ($(selector).length) ? $(selector) : false;
    },
    init: function() {
        this.geolocation.init();
        this.modules.init();
    },
    backendValidation: function($container, errors){
        if($container !== undefined && errors !== undefined){
            for (var name in errors) {
                var message = (typeof errors[name] == 'string') ? errors[name] : errors[name][0];
                var $el = $container.find('[name="' + name + '"]');
                $el.closest('label').addClass('error');
                message = '<b id="'+  name + '-error" class="error">' + message + '</b>';
                $container.find("#"+name+'-error').length ? $el.next('.error').remove() : '';
                !$container.find("#"+name+'-error').length ? $el.after(message) : '';
            }
            $('.form-preloader').stop( true, true).fadeOut( 200 );
            $('.form-preloader').remove();
            setTimeout(function(){
                for (var name in errors) {
                    var $el = $container.find("#"+name+'-error');
                    $el.closest('label').removeClass('error');
                    $el.remove();
                }
            }, 1500);
        }
    },

}
