var _Geolocation = {
    location: false,
    init: function() {
      if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          _Geolocation.location = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          };
        }, function(error) {
          _Geolocation.location = {
            latitude: 40.730610,
            longitude: -73.935242
          };
        });
      } else {
        _Geolocation.location = {
          latitude: 40.730610,
          longitude: -73.935242
        };
      }
    }
};
