<!DOCTYPE html>
<html lang="en">
<head>
	@include('admin.layouts._partials.head')
</head>

<body>
    @include('admin.layouts._partials.navbar')
    <!-- Page container -->
	<div class="page-container">
		<!-- Page content -->
		<div class="page-content">
            <!-- Main sidebar -->
            @include('admin.layouts._partials.sidebar')
            <!-- Main content -->
			<div class="content-wrapper">
                <div id="viewport"></div>
            </div>
        </div>
    </div>
	<!-- Modals -->
	@include('admin.layouts._partials.modals')
	@include('admin.layouts._partials.scripts')
</body>
</html>
