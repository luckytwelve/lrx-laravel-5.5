<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">
            {!! \Modules\Core\Helpers\Menu::build(config('admin.menu')) !!}
        </ul>
    </div>
</div>
