<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>LRX - Admin panel</title>
<link rel="shortcut icon"  href="/favicon.png" />
<!-- Global stylesheets -->
@include('admin.layouts._partials.styles')
<!-- /global stylesheets -->
