@if(isset($breadcrumbs) && $breadcrumbs && is_array($breadcrumbs))
    <div class="page-header">
        <div class="breadcrumb-line">
        	<ul class="breadcrumb">
                @foreach($breadcrumbs as $breadcrumb)
                    @if($loop->first)
                        <li>
                            <a href="{{ $breadcrumb['url'] }}">
                                <i class="icon-home2 position-left"></i>
                                {{ $breadcrumb['name'] or 'Home' }}
                            </a>
                        </li>
                    @elseif($loop->last)
                        <li class="active">
                            {{ $breadcrumb['name'] }}
                        </li>
                    @else
                        <li>
                            <a href="{{ $breadcrumb['url'] }}">
                                {{ $breadcrumb['name'] }}
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
@endif
