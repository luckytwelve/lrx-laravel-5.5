<script type="text/javascript" src="{{ asset('/assets/admin/js/theme.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/assets/admin/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ asset('/assets/admin/js/components.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/assets/admin/js/core.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/assets/admin/js/modules.min.js') }}"></script>

<!-- Custom JS files -->
<script type="text/javascript">
    ADM.managerPath = '/admin';

    $(document).ready(function() {
      ADM.init();
    });
</script>
