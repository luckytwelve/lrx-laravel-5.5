<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">{!! $name !!}</h5>
		<div class="heading-elements">
            @if(isset($buttons) && count($buttons) > 0)
            <ul>
                @foreach($buttons as $button)
                    {!! $button !!}
                @endforeach
        	</ul>
            @endif
    	</div>
	</div>

    @if(isset($description) && !empty($description))
    <div class="panel-body">
        {!! $description !!}
	</div>
    @endif

	<table class="table datatable-basic" id="{!! $id !!}">
		<thead>
			<tr>
				{!! $columns !!}
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
