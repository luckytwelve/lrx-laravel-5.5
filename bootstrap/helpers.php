<?php

use Illuminate\Support\Facades\Cache;
use Modules\Settings\Entities\Settings;
use \Modules\Core\Helpers\Permission as Permission;

/**
 * @param $name
 * @return mixed;
 */
if(!function_exists('getSetings')) {
    function getSetings($name) {
        $settings_db = (Cache::has('settings')) ? json_decode(Cache::get('settings'), true) : Settings::all();

        return (string) collect($settings_db)
            ->where('name', $name)
            ->pluck('values')
            ->first();
    }
}

if(!function_exists('managerHasOneOfPermissions')){
    function managerHasOneOfPermissions($permissions, $userID = false) {
        $permissions = is_array($permissions) ? $permissions : Array($permissions);
        return Permission::managerHasPermission($permissions, $userID);
    }
}

if(!function_exists('managerHasPermission')){
    function managerHasPermission($permissions, $userID = false) {
        $permissions = is_array($permissions) ? $permissions : Array($permissions);
        return Permission::managerHasPermission($permissions, $userID);
    }
}

